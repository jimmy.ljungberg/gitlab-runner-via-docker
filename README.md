# GitLab Runner via Docker

## Installation

```shell
git clone https://gitlab.com/jimmy.ljungberg/gitlab-runner-via-docker.git gitlab-runner
```

```shell
cat <<-EOF >> .env
DOCKER_IMAGE=gitlab/gitlab-runner:v15.11.0
GITLAB_URL=http://${GITLAB_HOSTNAME}/
GITLAB_RUNNER_REGISTRATION_TOKEN=
EOF
```

```shell
chmod 0600 .env
```

```shell
cat <<-EOF >> config.toml
concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800
EOF
```

```shell
chmod 0600 config.toml
```

Tilldela lämpliga värden till `GITLAB_HOSTNAME` och `GITLAB_RUNNER_REGISTRATION_TOKEN`.

## Tips

Det är väldigt vanligt att runners lämnar efter sig Docker-volymer som bara ligger och skräpar. Ett enkelt sätt att bli av med dom är att exekvera följande kommando:

```bash
sudo docker volume ls -qf dangling=true | xargs -r docker volume rm
```

## Docker utan HTTPS

Antag att din server har adress `192.168.192.128` och att den har Docker installerad. Dessutom har du installerat [GitLab via Docker](enligt mina instruktioner). Då är GitLab konfigurerad som Docker-register på port `5050`.

För att använda Docker utan HTTPS behöver du lägga till följande konfiguration:

Fil: `/etc/docker/daemon.json`
```json
{
  "insecure-registries": ["http://192.168.192.168:5050"]
}
```

## Problem

**Dangling docker volumes**  

Referens: https://stackoverflow.com/questions/66345607/docker-volume-cleanup-of-gitlab-runner

Exekvera kommandot:
```shell
docker volume ls -qf dangling=true | xargs --no-run-if-empty docker volume rm
```
