#!/usr/bin/env sh

. ./.env

docker exec -it gitlab-runner \
  gitlab-runner register \
    --non-interactive \
    --url "${GITLAB_URL}" \
    --docker-privileged \
    --registration-token "${GITLAB_RUNNER_REGISTRATION_TOKEN}" \
    --run-untagged="true" \
    --locked="false" \
    --executor "docker" \
    --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
    --docker-image bash:5.2.15
